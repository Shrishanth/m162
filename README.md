![TBZ Logo](x_gitressourcen/tbz_logo.png)

---

# Themen Schnelleinstieg

![m162-themen-uebersicht](./m162-themen-uebersicht.png)

## Daten und Datenstrukturen

- [Unstrukturierte und Strukturierte Daten](./Daten_Formate/StrukturierteDaten.md)
- [Merkmalstypen und Skalentypen](./Daten_Formate/Merkmalstypen_Skalentypen.md)
- [Datentypen](./Daten_Formate/Datentypen.md)
- [Datenstrukturen](./Daten_Formate/Datenstrukturen.md)

## Datenformate

- [Json und XML](./Daten_Formate/Json.md)
- [Dateiformate](./Daten_Formate/Dateiformate.md)
- [Zeichencodierung](./Daten_Formate/Zeichencodierung.md)
- [Open Data](./Daten_Formate/OpenData.md)

## Auswertungen, Diagramme und Excel

- [Excel Manual: Dateien importieren](./Auswertungen_Excel/Excel-txt-import.md)
- Excel Filterung/Sortierung
- Excel Formeln und Referenzen
- [Excel Manual: Diagramme erstellen](./Auswertungen_Excel/Diagramme-Erstellen.md)
- [Excel Linksammlung](./Auswertungen_Excel/Excel.md)

## Datenmodellierung

- [Mengenlehre](./Datenmodellierung/Theorie_Mengenlehre.md)
- [Theorie Datenmodellierung](./Datenmodellierung/Theorie_Datenmodellierung.md)
- [MySql Workbench](./Datenmodellierung/Theorie_MySqlWorkbench.md)

---

&copy;TBZ, 2022, Modul: m162