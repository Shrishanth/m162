![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# Strukturierte und Unstrukturierte Daten <!-- omit in toc -->

[TOC]

## Was ist Datenstruktur

In der Informatik und Softwaretechnik ist eine Datenstruktur ein Objekt zur **Speicherung und Organisation von Daten**. Es handelt sich um eine Struktur, weil die Daten in einer **bestimmten Art und Weise angeordnet und verknüpft** werden, um den Zugriff auf sie und ihre Verwaltung **effizient** zu ermöglichen.
Datenstrukturen sind nicht nur durch die enthaltenen Daten charakterisiert, sondern vor allem durch die Operationen auf diesen Daten, die **Zugriff und Verwaltung ermöglichen und realisieren**.

## Strukturierte Daten

Die Festlegung (Definition) von Datenstrukturen erfolgt im Allgemeinen durch eine exakte Beschreibung (Spezifikation) zur Datenhaltung und der dazu nötigen Operationen. Diese **konkrete Spezifikation** legt das allgemeine Verhalten der Operationen fest und abstrahiert damit von der konkreten Implementierung der Datenstruktur.

Wird der Schwerpunkt der Betrachtung auf die konkrete Implementierung der Operationen verschoben, so wird anstelle des Begriffs Datenstruktur auch häufig von einem abstrakten Datentyp gesprochen. Der Übergang von der Datenstruktur zu einem abstrakten Datentyp ist dabei nicht klar definiert, sondern hängt einzig von der Betrachtungsweise ab.

Von den meisten Datenstrukturen gibt es neben ihrer **Grundformen** ("Tabelle", "Beziehung", "Baume", "Register", "Kette", ... ) viele Spezialisierungen, die eigens für die Erfüllung einer bestimmten Aufgabe spezifiziert wurden. So sind beispielsweise **"B-Bäume"** als Spezialisierung der Datenstruktur **"Baum"** besonders gut für Implementierungen von Datenbanken geeignet.

Bei vielen Algorithmen **hängt der Ressourcenbedarf**, also sowohl die benötigte Laufzeit als auch der Speicherplatzbedarf, **von der Verwendung geeigneter Datenstrukturen** ab.

## Unstrukturierte Daten

In der Wirtschaftsinformatik und Computerlinguistik sind unstrukturierte Daten digitalisierte Informationen, die in einer **nicht formalisierten Struktur** vorliegen und auf die dadurch von Computerprogrammen nicht über eine einzelne Schnittstelle aggregiert zugegriffen werden kann. 

Beispiele sind **digitale Texte** in natürlicher Sprache und **digitale Tonaufnahmen** menschlicher Sprache.

## Einordnung

Unterschieden werden **unstrukturierte** Daten von **strukturierten** und **semi-strukturierten** Daten.

Semi-strukturierten Daten: *Betrachtet man eine E-Mail, so liegt diese in einer gewissen Struktur vor. Sie enthält einen Empfänger, einen Absender und eventuell einen Titel.  Der Inhalt der E-Mail selbst ist jedoch strukturlos.*


Die automatisierte Nutzbarkeit unstrukturierter Daten ist dadurch eingeschränkt, dass für sie **kein Datenmodell** und meist auch **keine Metadaten** (= zus. hinterlegte Information) vorliegen. Auch in Textdokumenten sind Metadaten und Daten vermischt. 

> Um Strukturen daraus zu gewinnen, ist **Modellierung** erforderlich. 

Im Unternehmensumfeld liegen oftmals wichtige Informationen in unstrukturierten Daten vor, deren Nichterfassung auch **rechtliche Probleme** verursachen kann. Daher befassen sich die Felder **Wissensmanagement und Datenmanagement** mit deren Integration und Verwaltung.

## Modellierung

Viele Daten sind bei ihrem Ursprung unstrukturiert. Sie gewinnen Struktur, indem sie durch menschliche Intervention **in ein Schema gebracht** werden (= Modellierung). Der Vorgang der Strukturierung kann Nachteile hervorrufen, da er oft mit einem **Informationsverlust** verbunden ist. 


## Behandlung von unstrukturierten Daten

Speziell für die Strukturierung der Daten können folgende Verfahren in Betracht gezogen werden:

1.	**Textanalyse und Textmining** existieren schon seit vielen Jahren auf dem Markt. Die Produkte dafür weisen eine solide Marktreife auf. Verschiedene kleine spezialisierte Hersteller haben Werkzeuge dafür entwickelt. Manche Business-Intelligence-Hersteller haben solche Technologien auf Druck des Marktes dazu gekauft. Textmining kann manuell, durch statistische Verfahren, über maschinelles Lernen oder über die Verarbeitung natürlicher Sprachen erfolgen. Es kann Begriffe und Konzepte in Thesauri liefern, die unabdingbar für zusätzliche Business-Intelligence-Analysen werden können.<br><br>
2.	**Maschinelles Lernen** basiert auf statistischen Verfahren wie Bayes-Klassifikatoren, neuronalen Netzwerken, oder latenter semantischer Analyse (LSA). Es ist viel effektiver als die klassischen statistischen Verfahren, jedoch nicht überall anwendbar. Es erfordert Überwachung und Training der Maschinen, und wie bei den Data-Mining Verfahren ist ein tiefes Wissen der Materie notwendig. <br><br>
3.	**Linguistische Verfahren** können schneller als maschinelles Lernen sein, und manchmal auch akkurater. Sie können Mehrdeutigkeit (Ambiguität) reduzieren, benötigen aber nach wie vor die menschliche Intervention. Hier sind die Modelle in Vergleich zu LSA und maschinellem Lernen einfacher zu verstehen.

## Organisation von Daten

Neben der **Form**, in welcher sich die Information präsentiert (Text, Ton, Bild, ...) ist auch die **Art**, in der die Information organisiert (sortiert) ist, sehr wichtig. 

Die Organisation bestimmt, **wie auf Information zugegriffen** werden kann. Sie bestimmt, ob überhaupt gesucht werden kann, ob sich Elemente direkt vergleichen lassen, usw. 
Als Synonyme für Organisation gelten in diesem Zusammenhang auch Struktur und Ordnung. 

Folgende Tabelle mag die Bedeutung der Organisation von Information auf andere Faktoren verdeutlichen:

 

|                                 | **schwach strukturiert**     | **mässig strukturiert**        | **stark strukturiert**               |
| ------------------------------- | ---------------------------- | ------------------------------ | ------------------------------------ |
| **Daten:**                      | beliebiger  Text             | Tabellen,  Listen              | Adressen,  Lagerverwaltung usw.      |
| **Suchen:**                     | schlecht                     | gut  nach sortiertem Kriterium | nach  vielen Kriterien möglich       |
| **Werkzeug:**                   | gedruckte  Unterlagen        | Karteikasten                   | Archiv                               |
| **Einfügen:**                   | beliebige  Position, einfach | nach  Position sortiert        | Position  und Beziehungen, aufwendig |
| **elektronische Verarbeitung:** | Suchmaschine                 | Tabellenkalkulation            | Datenbank                            |
| **Software-Tools**              | **Word**                     | **Excel**                      | **Access / Workbench**                           |



## Strukturierungsgrad von Daten

**Textdaten** erlauben das Verarbeiten von unstrukturierten Daten. Ausser dem Zeichensatz welcher zur Verfügung steht gibt es keine Einschränkungen an die Struktur der Daten. Sehr beschränkt sind dafür die Auswertemöglichkeiten eines Textes. <br>
Würde der Börsenkurs in Worten beschrieben, könnte er nicht als Liniendiagramm dargestellt werden.

**Tabellendaten** sind mässig strukturiert. Die Strukturierung sieht Zeilen und Spalten vor und verlangt, dass Daten bei der Eingabe als Typ «Text» oder als Typ «Zahl» definiert werden. Geben Sie in Excel zwar Zahlen ein, definieren sie aber als Datentyp «Text», kann Excel kein Diagramm damit erstellen. <br>
Durch den stärkeren Strukturierungsgrad sind dafür die Auswertemöglichkeiten besser. Sortierte Listen nach verschiedenen Kriterien und schnelles Auswerten diverser Zahlenreihen sind die Stärken mässig strukturierter Daten.

**Datenbanken** bestehen aus zwei bis vielen hundert Tabellen. In den Tabellen werden die Daten stark strukturiert in Form von Datensätzen abgelegt. Die Tabellen stehen auch in genau definierter Beziehung zueinander. Die Daten in den einzelnen Tabellen sind also voneinander abhängig. Dadurch erreicht man sehr gute Auswertemöglichkeiten. Hingegen wäre ein Roman oder ein bebildertes Märchenbuch in Datenbankform nur schwierig lesbar.

### Merkmale stark strukturierter Daten sind:

- Sie sind vollständig sortiert oder sortierbar.
- Sie können nach mehreren Kriterien sortiert werden.
- Das Suchen nach einem sortierten Kriterium ist einfach und schnell.
- Das Einfügen von Daten ist aufwändig, da die passende Stelle gesucht werden muss.

### Merkmale schwach strukturierter Daten sind:

- Sie sind nicht sortiert.
- Das Suchen nach sortierten Werten ist aufwändig.
- Das Einfügen von Daten geht einfach und ist fast überall möglich.

---

&copy;TBZ, 2021, Modul: m162
