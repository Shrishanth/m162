![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Arbeitsumgebung I": ERD Aufgrund von Tabellen erstellen

In dieser Übung erstellen wir ein ERD aufgrund von vorhandenen Tabellen und Daten. Sie üben also die Struktur aufgrund von Daten zu erkennen und zu erstellen.

#### Aufgabe

Schauen sie sich die folgenden Daten an und erstellen sie das logische ERD in **draw.io** und anschliessend auch das physische in **MySql Workbench**.

**Hinweis**: Erinnern sie sich daran, dass im logischen/physischen ERD nur 1:m Beziehungen existieren. Sie kriegen die korrekten Lösungen also indem sie schauen welche FS (Fremdschlüssel) existieren.

![daten](x_gitressourcen/Arbeitsumgebung_I.png)

#### Zeit und Form

- 20 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162
