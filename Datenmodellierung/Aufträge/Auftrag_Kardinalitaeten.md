![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: Definieren sie die Kardinalitäten

In dieser Übung geht es darum, dass sie sich den Kardinalitäten beschäftigen. Wenn die Beziehung mehrdeutig ist, definieren sie welche Beziehung darstellen.

#### Aufgabe 1

Definieren sie die Kardinalitäten für die folgenden Beziehungen **ohne** Bezeichnung

![Kardinalitäten_a](x_gitressourcen/Kardinalität_Uebungen_a.png)

#### Aufgabe 2

Definieren sie die Kardinalitäten für die folgenden Beziehungen **mit** Bezeichnung

![Kardinalitäten_b](x_gitressourcen/Kardinalität_Uebungen_b.png)

#### Aufgabe 3

Definieren sie die **Kardinalitäten** und **Beziehungsbezeichnungen** für die folgenden Beziehungen zwischen immer den gleichen Entitäten.

![Kardinalitäten_b](x_gitressourcen/Kardinalität_Uebungen_c.png)
  
#### Zeit und Form

- 30 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162