![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# Recap Tabelle

Füllen Sie die Lücken im [Dokument](./Tabellenterminologie.pdf) aus. Mit dieser Uebung lernen Sie die Tabellenterminologie kennen.

**Zeit**: 15 Minuten

**Form**: in 2er Gruppen

**Tools**: -

---

&copy;TBZ, 2021, Modul: m162