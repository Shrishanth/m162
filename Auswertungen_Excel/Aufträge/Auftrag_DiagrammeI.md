![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: Parteizugehörigkeit

Verwenden sie als Grundlage die folgende Excel-Datei: [Auftrag_DiagrammeI_Quelle.xlsx](Auftrag_DiagrammeI_Quelle.xlsx) (Quelle: <https://www.bfs.admin.ch/bfs/de/home/statistiken/politik/wahlen/nationalratswahlen/parteistaerken.assetdetail.11048421.html>).

**Hinweis**: Alle relevanten Schritte wie sie vorgehen, finden sie auf dem [Merkblatt](../Diagramme-Erstellen.md).

#### Aufgaben

- Erstellen sie ein Liniendiagramm über den Stärkeverlauf der **Partei SP** über die Zeit
- Erstellen sie ein Säulendiagramm, welches alle Parteien gegenüberstellt für das **Jahr 2019**
- Erstellen sie ein Säulendiagramm, welches die beiden Parteien **SP und SVP** vergleicht **in den Jahren 1919 und 2019**. Sie dürfen die Daten auch neu zusammenkopieren, um es zu vereinfachen (Notwendig ist es nicht).

#### Zeit und Form

- 20 Minuten
- Abgabe als Excel-Daten
- Individuell

---

&copy;TBZ, 2021, Modul: m162